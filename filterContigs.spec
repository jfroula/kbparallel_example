/*
A KBase module: filterContigs
This sample module contains one small method - filter_contigs.
*/

module filterContigs {
    /* An X/Y/Z style reference
    */
    typedef string obj_ref;

    /*
        A 'typedef' can also be used to define compound or container
        objects, like lists, maps, and structures.  The standard KBase
        convention is to use structures, as shown here, to define the
        input and output of your function.  Here the input is a
        reference to the Assembly data object, a workspace to save
        output, and a length threshold for filtering.

        To define lists and maps, use a syntax similar to C++ templates
        to indicate the type contained in the list or map.  For example:

            list <string> list_of_strings;
            mapping <string, int> map_of_ints; 
    */
    typedef structure {
        list<obj_ref> reads_list;
        string workspace_name;
    } FilterContigsParams;

    /*
        input params for runner that actually does work (do_the_work)
    */
    typedef structure {
        string input_fastq;
    } RunnerParams;
          
    /*
        output params for runner that actually does work (do_the_work)
    */
    typedef structure {
        string results;
    } RunnerOutput;
    
    
    /*
        Here is the definition of the output of the function.  The output
        can be used by other SDK modules which call your code, or the output
        visualizations in the Narrative.  'report_name' and 'report_ref' are
        special output fields- if defined, the Narrative can automatically
        render your Report.
    */
    typedef structure {
        string report_name;
        string report_ref;
    } FilterContigsResults;
    
    
    /*
        The actual function is declared using 'funcdef' to specify the name
        and input/return arguments to the function.  For all typical KBase
        Apps that run in the Narrative, your function should have the 
        'authentication required' modifier.
    */
    funcdef align_reads_to_assembly_app(FilterContigsParams params)
        returns (FilterContigsResults output) authentication required;
    
    
    /*
        This will actually do the work, i.e job runner on single machine
    */
        funcdef do_the_work(RunnerParams params)
            returns (RunnerOutput results) authentication required;
        
        
};
