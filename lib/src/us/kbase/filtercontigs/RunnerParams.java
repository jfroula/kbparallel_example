
package us.kbase.filtercontigs;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * <p>Original spec-file type: RunnerParams</p>
 * <pre>
 * input params for runner that actually does work (do_the_work)
 * </pre>
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "input_fastq"
})
public class RunnerParams {

    @JsonProperty("input_fastq")
    private String inputFastq;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("input_fastq")
    public String getInputFastq() {
        return inputFastq;
    }

    @JsonProperty("input_fastq")
    public void setInputFastq(String inputFastq) {
        this.inputFastq = inputFastq;
    }

    public RunnerParams withInputFastq(String inputFastq) {
        this.inputFastq = inputFastq;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return ((((("RunnerParams"+" [inputFastq=")+ inputFastq)+", additionalProperties=")+ additionalProperties)+"]");
    }

}
