# -*- coding: utf-8 -*-
#BEGIN_HEADER
# The header block is where all import statments should live
import os
import sys
import uuid
from Bio import SeqIO
from pprint import pprint, pformat
from DataFileUtil.DataFileUtilClient import DataFileUtil
from filterContigs.Utils.filterContigsUtil import kbparallelTest
#END_HEADER


class filterContigs:
    '''
    Module Name:
    filterContigs

    Module Description:
    A KBase module: filterContigs
    '''

    ######## WARNING FOR GEVENT USERS ####### noqa
    # Since asynchronous IO can lead to methods - even the same method -
    # interrupting each other, you must be *very* careful when using global
    # state. A method could easily clobber the state set by another while
    # the latter method is running.
    ######################################### noqa
    VERSION = "1.2.5"
    GIT_URL = "git@gitlab.com:jfroula/kbparallel_example.git"
    GIT_COMMIT_HASH = "82c61cfa2812ee5fbf4851d92dec58d7c87ad664"

    #BEGIN_CLASS_HEADER
    # Class variables and functions can be defined in this block
    #END_CLASS_HEADER

    # config contains contents of config file in a hash or None if it couldn't
    # be found
    def __init__(self, config):
        #BEGIN_CONSTRUCTOR
        
        # Any configuration parameters that are important should be parsed and
        # saved in the constructor.
        self.callback_url = os.environ['SDK_CALLBACK_URL']
        self.config = config

        self.scratch = os.path.abspath(config['scratch'])
        self.workspace_url = config['workspace-url']
        self.srv_wiz_url = config['srv-wiz-url']
        self.callback_url = os.environ['SDK_CALLBACK_URL']
        self.dfu = DataFileUtil(self.callback_url)
        
        #END_CONSTRUCTOR
        pass


    def align_reads_to_assembly_app(self, ctx, params):
        """
        The actual function is declared using 'funcdef' to specify the name
        and input/return arguments to the function.  For all typical KBase
        Apps that run in the Narrative, your function should have the 
        'authentication required' modifier.
        :param params: instance of type "FilterContigsParams" (A 'typedef'
           can also be used to define compound or container objects, like
           lists, maps, and structures.  The standard KBase convention is to
           use structures, as shown here, to define the input and output of
           your function.  Here the input is a reference to the Assembly data
           object, a workspace to save output, and a length threshold for
           filtering. To define lists and maps, use a syntax similar to C++
           templates to indicate the type contained in the list or map.  For
           example: list <string> list_of_strings; mapping <string, int>
           map_of_ints;) -> structure: parameter "reads_list" of list of type
           "obj_ref" (An X/Y/Z style reference), parameter "workspace_name"
           of String
        :returns: instance of type "FilterContigsResults" (Here is the
           definition of the output of the function.  The output can be used
           by other SDK modules which call your code, or the output
           visualizations in the Narrative.  'report_name' and 'report_ref'
           are special output fields- if defined, the Narrative can
           automatically render your Report.) -> structure: parameter
           "report_name" of String, parameter "report_ref" of String
        """
        # ctx is the context object
        # return variables are: output
        #BEGIN align_reads_to_assembly_app
        annot_runner = kbparallelTest(self.config)
        output = annot_runner.run_parallel_jobs(params)
        #END align_reads_to_assembly_app

        # At some point might do deeper type checking...
        if not isinstance(output, dict):
            raise ValueError('Method align_reads_to_assembly_app return value ' +
                             'output is not type dict as required.')
        # return the results
        return [output]

    def do_the_work(self, ctx, params):
        """
        This will actually do the work, i.e job runner on single machine
        :param params: instance of type "RunnerParams" (input params for
           runner that actually does work (do_the_work)) -> structure:
           parameter "input_fastq" of String
        :returns: instance of type "RunnerOutput" (output params for runner
           that actually does work (do_the_work)) -> structure: parameter
           "results" of String
        """
        # ctx is the context object
        # return variables are: results
        #BEGIN do_the_work
        results={}
        
        # copy over the fastq file from workspace to local scratch.
        annot_runner = kbparallelTest(self.config)
        myfile = annot_runner.stage_reads_list_file(params['fastq_name'])

        # Do something with the file. We'll just calculate number of line.
        first_line=''
        with open(myfile[0]) as f:
            first_line = f.readline()
    
        # this line will only be visible for jobs run on local node and not the njws ones.
        print('IF YOU SEE THIS THEN YOU ARE RUNNING ON MASTER: first line in FILE: {}\nLINE {}'.format(myfile[0], first_line))
        
        # Need to return a dictionary. This is the results from one task.
        results = {'first_line': first_line}
        
        # Note: If you use KBParallel to create parallel tasks, you should design your strategy
        # such that any file that is created by each task doesn't have to be copied back to the
        # master node since this could bog down the system. It is prefered that you only send back to
        # master, the essential data summed up in the data structure that is returned.
        # There is too much overhead involved if you had to upload a file to shock from
        # the slave and then back down to the master..
        
        #END do_the_work

        # At some point might do deeper type checking...
        if not isinstance(results, dict):
            raise ValueError('Method do_the_work return value ' +
                             'results is not type dict as required.')
        # return the results
        return [results]
    def status(self, ctx):
        #BEGIN_STATUS
        returnVal = {'state': "OK",
                     'message': "",
                     'version': self.VERSION,
                     'git_url': self.GIT_URL,
                     'git_commit_hash': self.GIT_COMMIT_HASH}
        #END_STATUS
        return [returnVal]
