
import os
import copy
import sys
from Bio import SeqIO
from pprint import pprint, pformat
from KBaseReport.KBaseReportClient import KBaseReport
from KBParallel.KBParallelClient import KBParallel
from SetAPI.SetAPIServiceClient import SetAPI
from Workspace.WorkspaceClient import Workspace
from ReadsUtils.ReadsUtilsClient import ReadsUtils

import uuid

class kbparallelTest:
    '''
    Module Name:
    kbparallelTest

    Module Description:
    A KBase module: kbparallelTest
    '''

    ######## WARNING FOR GEVENT USERS ####### noqa
    # Since asynchronous IO can lead to methods - even the same method -
    # interrupting each other, you must be *very* careful when using global
    # state. A method could easily clobber the state set by another while
    # the latter method is running.
    ######################################### noqa
    VERSION = "0.0.1"
    GIT_URL = ""
    GIT_COMMIT_HASH = ""

    #BEGIN_CLASS_HEADER
    # Class variables and functions can be defined in this block
    #END_CLASS_HEADER

    # config contains contents of config file in a hash or None if it couldn't
    # be found
    def __init__(self, config):
    # def __init__(self, scratch, workspace_url, callback_url, srv_wiz_url, provenance):
        self.callback_url = os.environ['SDK_CALLBACK_URL']
        # self.config = config

        self.scratch = os.path.abspath(config['scratch'])
        self.workspace_url = config['workspace-url']
        # self.srv_wiz_url = config['srv-wiz-url']
        # self.callback_url = os.environ['SDK_CALLBACK_URL']
        
        # self.scratch = scratch
        # self.workspace_url = workspace_url
        # self.callback_url = callback_url
        # self.srv_wiz_url = srv_wiz_url
        # self.provenance = provenance
        self.ws = Workspace(self.workspace_url)
        self.ru = ReadsUtils(self.callback_url)

        # from bowtie
        self.parallel_runner = KBParallel(self.callback_url)

    def stage_reads_list_file(self, reads_list):
        """
        stage_reads_list_file: download fastq file associated to reads to scratch area
                          and write result_file_path to file
        """
        print('Processing reads object list: {}'.format(reads_list))
        
        result_file_path = []

        # getting from workspace and writing to scratch. The 'reads' dictionary now has file paths to scratch.
        reads = self.ru.download_reads({'read_libraries': reads_list, 'interleaved': None})['files']

        # reads_list is the list of file paths on workspace? (i.e. 12804/1/1).
        # "reads" is the hash of hashes where key is "12804/1/1" or in this case, read_obj and
        # "files" is the secondary key. The tertiary keys are "fwd" and "rev", as well as others.
        for read_obj in reads_list:
            files = reads[read_obj]['files']    # 'files' is dictionary where 'fwd' is key of file path on scratch.
            result_file_path.append(files['fwd'])
            if 'rev' in files and files['rev'] is not None:
                result_file_path.append(files['rev'])

        return result_file_path
    
    def run_parallel_jobs(self, params):
        """
        The actual function is declared using 'funcdef' to specify the name
        and input/return arguments to the function.  For all typical KBase
        Apps that run in the Narrative, your function should have the 
        'authentication required' modifier.
        :param params: instance of type "FilterContigsParams" (A 'typedef'
           can also be used to define compound or container objects, like
           lists, maps, and structures.  The standard KBase convention is to
           use structures, as shown here, to define the input and output of
           your function.  Here the input is a reference to the Assembly data
           object, a workspace to save output, and a length threshold for
           filtering. To define lists and maps, use a syntax similar to C++
           templates to indicate the type contained in the list or map.  For
           example: list <string> list_of_strings; mapping <string, int>
           map_of_ints;) -> structure: parameter "assembly_input_ref" of type
           "assembly_ref" (A 'typedef' allows you to provide a more specific
           name for a type.  Built-in primitive types include 'string',
           'int', 'float'.  Here we define a type named assembly_ref to
           indicate a string that should be set to a KBase ID reference to an
           Assembly data object.), parameter "workspace_name" of String,
           parameter "min_length" of Long
        :returns: instance of type "FilterContigsResults" (Here is the
           definition of the output of the function.  The output can be used
           by other SDK modules which call your code, or the output
           visualizations in the Narrative.  'report_name' and 'report_ref'
           are special output fields- if defined, the Narrative can
           automatically render your Report.) -> structure: parameter
           "report_name" of String, parameter "report_ref" of String,
           parameter "assembly_output" of type "assembly_ref" (A 'typedef'
           allows you to provide a more specific name for a type.  Built-in
           primitive types include 'string', 'int', 'float'.  Here we define
           a type named assembly_ref to indicate a string that should be set
           to a KBase ID reference to an Assembly data object.), parameter
           "n_initial_contigs" of Long, parameter "n_contigs_removed" of
           Long, parameter "n_contigs_remaining" of Long
        """

        #
        # Set up tasks to run. I'm creating 2 tasks, 1 for each fastq file. 
        # The task_params are what get sent to filterContigs.do_the_work().
        # Note that the params are the same for all tasks except the fastq reference.
        #
        tasks = []

        for fastq_name in params['reads_list']:
            # task_params = copy.deepcopy(params)
            task_params={}
            task_params['fastq_name'] = [fastq_name]
            
            tasks.append( {'module_name': 'filterContigs',
                            'function_name': 'do_the_work',
                            'version': 'dev',
                            'parameters': task_params
                          } )
            
        # kbparallel requires these params to shape the jobs on the submit & local nodes
        batch_run_params = {'tasks': tasks,
                            'runner': 'parallel',
                            'concurrent_local_tasks': 1,
                            'concurrent_njsw_tasks': 1,
                            'max_retries': 2}
        
        print('KBPARALLEL PARAMS')
        pprint(batch_run_params)
        
        # Run parallel tasks.  This runs "filterContigs.do_the_work".
        # The output is a list of dictionaries. Each element in the list is from one task.
        # The results includes other things than just what you returned in "filterContigs.do_the_work"
        # like error messages, jobID, etc.
        output = self.parallel_runner.run_batch(batch_run_params)
        
        print('OUTPUT FROM PARALLEL_RUNNER.RUN_BATCH')
        pprint(output)
        
        # Each task just grabbed the first line of the fastq file, so let's print it out here
        print('RESULTS FROM EACH TASK')
        for result_hash in output['results']:
            line     = result_hash['final_job_state']['result'][0]['first_line']
            location = result_hash['result_package']['run_context']['location']
            print('job on {}: first line of fastq: {}'.format(location, line))
        
        return output
    
    def status(self, ctx):

        returnVal = {'state': "OK",
                     'message': "",
                     'version': self.VERSION,
                     'git_url': self.GIT_URL,
                     'git_commit_hash': self.GIT_COMMIT_HASH}

        return [returnVal]
