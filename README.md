## summary
This app uses the Kbase utility module "KBParallel" that allows jobs to be run in parallel. It runs three jobs in parallel. Each job creates a <file>.txt and thats it.  It should contain the minimum code to accomplish this task so it should be a good starting point for people trying to implement KBParallel functionality.

### testing the app
There are two ways you can run this app, locally or from your narrative on https://narrative.kbase.us/.  

#### run locally
To compile and run the example App implementation, run:

    cd filterContigs
    make          (required after making changes to $module_name.spec)
    kb-sdk test   (will require setting test user account credentials in test_local/test.cfg)

##### Hint
while you test your code, you should run jobs on local node by setting concurrent_njsw_tasks: 0. Then when you are satisfied this works, remember to register you repository on appdev and then set concurrent_njsw_tasks: to some value > 0.

```
batch_run_params = {'tasks': tasks,
                            'runner': 'parallel',
                            'concurrent_local_tasks': 1,
                            'concurrent_njsw_tasks': 1,
                            'max_retries': 2}
```

#### run on appdev
From your narrative on https://narrative.kbase.us/, search for "kbparallel example" in the "beta" space in and the app should be located under the category "Utilities".  You can then run the app by supplying it with TWO fastq files.  
Check the `Job Status` and the first line of each file will be printed out as final output. One fastq gets processed on local node and the other on a njsw slave node.  

### expected results (check "Job Status" if run in appdev)
Regardless how you run the app, the output should end with something this:
```
RESULTS FROM EACH TASK
job on local: first line of fastq: @test_mRNA_150_290_0/1

job on njsw: first line of fastq: @test_mRNA_150_290_0/2
```
One job was run on 'local' and one on 'njsw'  


If you want to see the essential code, not in a working example, you can see the README for KBParallel:
https://github.com/kbaseapps/KBParallel

## important things to know about KBParallel
#### I) Stage files on the remote node
For example, in this example (filterContigs app), I uploaded the fastq reads from shock to scratch on each node (see filterContigsImpl.stage_reads_list_file).  

#### II) Don't try to copy files produced from each task back to the master
You should design your strategy such that any file that is created by each task doesn't have to be copied back to the
master node since this could bog down the system. It is prefered that you only send back to master, the essential data summed up in the data structure that is returned. There is too much overhead involved if you had to upload a file to shock from the slave and then back down to the master.
        
#### III) register your module
kbparallel runs a function in parallel. This function must be registered in 'dev','release', or 'beta' in kbase. One or more tasks can be run on a local submit host and additional tasks can be submitted to "njsw" nodes, one task per node.   

Remember: When you run `kb-sdk test` the function that is being run in parallel will be run on a Kbase server. Thus, when you change something in this code, you need to re-register (i.e. update) your repository in appdev.  

#### IV) tasks
you create tasks (list of dictionaries) that will be run in parallel like this:
```
tasks = [{'module_name': 'filterContigs',
         'function_name': 'do_the_work',
         'version': 'dev',
         'parameters': { ... }
         },
         ...
         ]
```

##### An explanation of the above datastructure:  
| key | description |
|------|-----------|
|**filterContigs**|is the name of a registered module; has to be in 'dev'|'beta'|'release' (I've only tried 'dev').| 
|**do\_the\_work**|is the function that KBParallel will run in parallel.| 
|**'dev'**|is where you registered the module|   
|**parameters**|are what get sent to **do\_the\_work**.|

#### V) specifying number of nodes
you can specify up to 5 nodes in total, which includes the local, submit node (so 4 slave "njsw" nodes).  Of course the number of njsw nodes should be dynamic. You can also specify to run multiple jobs on the local node. (see README for https://github.com/kbaseapps/KBParallel).  

#### VI) access to output
If you run your app locally, remember that the njsw jobs are running on a Kbase remote machine and therefore any print statements in, for example, **do\_the\_work** will not be seen.  Any output you want access to must be returned by the "return" statement. And remember not to plan on copying any files produced from the remote jobs back to the master.  

#### VII) output datastructure
The output from the function that gets run in parallel (i.e **do\_the\_work**) is saved in a special data structure.  Again, see the README for KBParallel.  




