# -*- coding: utf-8 -*-
import unittest
import os  # noqa: F401
import json  # noqa: F401
import time
import requests
import shutil

from os import environ
try:
    from ConfigParser import ConfigParser  # py2
except:
    from configparser import ConfigParser  # py3

from pprint import pprint  # noqa: F401

from biokbase.workspace.client import Workspace as workspaceService
from filterContigs.filterContigsImpl import filterContigs
from filterContigs.filterContigsServer import MethodContext
from filterContigs.authclient import KBaseAuth as _KBaseAuth

# from AssemblyUtil.AssemblyUtilClient import AssemblyUtil
from ReadsUtils.ReadsUtilsClient import ReadsUtils

class filterContigsTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        token = environ.get('KB_AUTH_TOKEN', None)
        config_file = environ.get('KB_DEPLOYMENT_CONFIG', None)
        cls.cfg = {}
        config = ConfigParser()
        config.read(config_file)
        for nameval in config.items('filterContigs'):
            cls.cfg[nameval[0]] = nameval[1]
        # Getting username from Auth profile for token
        authServiceUrl = cls.cfg['auth-service-url']
        auth_client = _KBaseAuth(authServiceUrl)
        user_id = auth_client.get_user(token)
        # WARNING: don't call any logging methods on the context object,
        # it'll result in a NoneType error
        cls.ctx = MethodContext(None)
        cls.ctx.update({'token': token,
                        'user_id': user_id,
                        'provenance': [
                            {'service': 'filterContigs',
                             'method': 'please_never_use_it_in_production',
                             'method_params': []
                             }],
                        'authenticated': 1})
        # get workspace info
        cls.wsURL = cls.cfg['workspace-url']
        cls.wsClient = workspaceService(cls.wsURL)
        suffix = int(time.time() * 1000)
        wsName = "test_metabat_" + str(suffix)
        cls.ws_info = cls.wsClient.create_workspace({'workspace': wsName})
        
        cls.serviceImpl = filterContigs(cls.cfg)
        cls.scratch = cls.cfg['scratch']
        cls.callback_url = os.environ['SDK_CALLBACK_URL']
        
        cls.ru = ReadsUtils(os.environ['SDK_CALLBACK_URL'], token=token)
        cls.prepare_data()
        
    @classmethod
    def tearDownClass(cls):
        if hasattr(cls, 'wsName'):
            cls.wsClient.delete_workspace({'workspace': cls.wsName})
            print('Test workspace was deleted')
            
    @classmethod
    def prepare_data(cls):
        """
        Lets put everything on workspace
        """
        pe1_reads_filename = 'reads_1.fq'
        pe1_reads_path = os.path.join(cls.scratch, pe1_reads_filename)
        
        # gets put on scratch. "work/tmp" is scratch        
        shutil.copy(os.path.join("data", "bt_test_data", pe1_reads_filename), pe1_reads_path)
        
        int1_reads_params = {
            'fwd_file': pe1_reads_path,
            'sequencing_tech': 'Unknown',
            'wsname': cls.ws_info[1],
            'name': 'MyInterleavedLibrary1',
            'interleaved': 'true'
        }
        
        #from scratch upload to workspace
        cls.int1_oldstyle_reads_ref = cls.ru.upload_reads(int1_reads_params)['obj_ref']                    
        
        # READS 2
        # building Interleaved library
        pe2_reads_filename = 'reads_2.fq'
        pe2_reads_path = os.path.join(cls.scratch, pe2_reads_filename)
        
        # gets put on scratch. "work/tmp" is scratch        
        shutil.copy(os.path.join("data", "bt_test_data", pe2_reads_filename), pe2_reads_path)
        
        int2_reads_params = {
            'fwd_file': pe2_reads_path,
            'sequencing_tech': 'Unknown',
            'wsname': cls.ws_info[1],
            'name': 'MyInterleavedLibrary2',
            'interleaved': 'true'
        }
                
        #from scratch upload to workspace
        cls.int2_oldstyle_reads_ref = cls.ru.upload_reads(int2_reads_params)['obj_ref']

    def getImpl(self):
        return self.__class__.serviceImpl

    def getContext(self):
        return self.__class__.ctx
    
    # NOTE: According to Python unittest naming rules test method names should start from 'test'. # noqa
    def test_filter_contigs_ok(self):        
        
        # pprint(self.ws_info)
        
        params = {'reads_list': [self.int1_oldstyle_reads_ref, self.int2_oldstyle_reads_ref],
                  'output_workspace': self.ws_info[1]
                  }
        
        res = self.getImpl().align_reads_to_assembly_app(self.getContext(), params)[0]
        
        


    
